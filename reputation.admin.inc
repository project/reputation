<?php

/**
 * @file
 * Configuration page for Reputation module.
 */

/**
 * Form to add voting widget to comments per node type.
 */
function reputation_settings_form($form, &$form_state) {
  // Construct an array of options for selecting vocabulary.
  $vocabulary_options = array('0' => '-- ' . t('Select vocabulary') . ' --');
  $vocabularies = taxonomy_vocabulary_get_names();
  foreach ($vocabularies as $vocabulary) {
    $vocabulary_options[$vocabulary->vid] = $vocabulary->name;
  }

  // Form for selecting vocabulary to hold types of reputation.
  $form['reputation_types'] = array(
    '#type' => 'select',
    '#title' => t('Types of reputation'),
    '#options' => $vocabulary_options,
    '#default_value' => variable_get('reputation_types', 0),
    '#description' => t('Taxonomy vocabulary to hold reputation types as terms'),
    '#weight' => -1,
  );

  // Add links for adding terms and creating a new vocabulary.
  $destination = drupal_get_destination();
  if (variable_get('reputation_types', 0) != 0) {
    $current_vocabulary = taxonomy_vocabulary_load(variable_get('reputation_types', 0));
    $form['reputation_types']['#description'] .= ' · ' . l(
      t('Add a term to the current vocabulary'),
      'admin/structure/taxonomy/' . $current_vocabulary->machine_name . '/add',
      array('query' => array($destination))
    );
  }
  $form['reputation_types']['#description'] .= ' · ' . l(
    t('Create a new vocabulary'),
    'admin/structure/taxonomy/add',
    array('query' => array($destination))
  );

  // Form element to define node types on whose comments to show voting widget.
  $form['reputation_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allow voting on comments to these content types'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('reputation_node_types', array('')),
    '#description' => t("A voting widget will be available on these content types to vote for user's contribution."),
  );

  return system_settings_form($form);
}
