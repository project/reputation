(function ($) {
Drupal.behaviors.reputation = {
  attach: function (context) {
        $('.reputation-widget').delegate('.reputation-button', 'click', function(event){
            var $rmenu = $(this).next().children();
            $('<div class="over"></div>').css({left : '0px', top : '0px', position: 'fixed', width:'100%', height: '100%', zIndex: '10000' }).click(function() {
              $(this).remove();
              $rmenu.hide();
              return false;
              }).appendTo(document.body);
              var offset = $(this).position();
            $(this).next().children().css({ left: offset.left, top: offset.top, zIndex: '10001' }).show('fast');
            return false;
         });

        $('.reputation-widget').delegate('.reputation-link', 'click', function() {
            var widget = $(this);
            var voteSaved = function (data) {
              $(widget).closest('.reputation-widget').siblings('.votes').html(data.results);
              $(widget).closest('.reputation-widget').html(data.voted);
              }

            $.ajax({
              type: 'POST',
              url: this.href,
              dataType: 'json',
              data: 'js=1',
              success: voteSaved
              });
            $('.rlist').hide();
            $('.over').hide();
            return false;
           });

        $('.votes').delegate('.show-more', 'click', function() {
            var link = $(this);
            var moreVotes = function (data) {
              $(link).closest('.votes').html(data.results);
            }

            $.ajax({
              type: 'POST',
              url: this.href,
              dataType: 'json',
              data: 'js=1',
              success: moreVotes
              });
            return false;
           });
        }
};
})(jQuery);
