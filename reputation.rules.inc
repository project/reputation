<?php
/**
 * @file
 * Rules integration for Reputation module
 */

/**
 * Implements hook_rules_event_info().
 */
function reputation_rules_event_info() {
  $events=array();
  $events ['rules_user_reputation_increased'] = array(
      'label' => t('User reputation increased'),
      'module' => 'reputation',
      'group' => 'Reputation' ,
      'variables' => array(
        'user' => array('type' => 'user', 'label' => t('User who received reputation points.')),
        'type' => array('type' => 'taxonomy_term', 'label' => t('Reputation type.')),
        'value' => array('type' => 'integer', 'label' => t('Current user reputation.')),
      ),
  );
  return $events;
}
